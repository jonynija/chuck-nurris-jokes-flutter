import 'dart:io';
import 'dart:math';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:google_mobile_ads/google_mobile_ads.dart';

import 'package:chuck_norris_jokes/core/constants.dart';
import 'package:chuck_norris_jokes/domain/entities/joke.dart';
import 'package:chuck_norris_jokes/domain/repositories/random_joke_repository.dart';
import 'package:chuck_norris_jokes/presentation/routes/route_names.dart';

class ChuckNorrisJokesViewModel extends ChangeNotifier {
  ChuckNorrisJokesViewModel({
    required RandomJokeRepository repository,
  }) : _repository = repository {
    _init();
  }

  final RandomJokeRepository _repository;

  final listKey = GlobalKey<AnimatedListState>();

  List<Joke> jokes = [];

  BannerAd? myBanner;

  Future<void> _init() async {
    if (!kIsWeb && Platform.isAndroid) {
      _adConfig();
    }

    _fetchNewJoke();
  }

  Future<void> _adConfig() async {
    try {
      final BannerAdListener listener = BannerAdListener(
        // Called when an ad is successfully received.
        onAdLoaded: (Ad ad) {
          debugPrint('Ad loaded.');
        },
        // Called when an ad request failed.
        onAdFailedToLoad: (Ad ad, LoadAdError error) {
          // Dispose the ad here to free resources.
          ad.dispose();
          debugPrint('Ad failed to load: $error');
        },
        // Called when an ad opens an overlay that covers the screen.
        onAdOpened: (Ad ad) {
          debugPrint('Ad opened.');
        },
        // Called when an ad removes an overlay that covers the screen.
        onAdClosed: (Ad ad) {
          debugPrint('Ad closed.');
        },
        // Called when an impression occurs on the ad.
        onAdImpression: (Ad ad) {
          debugPrint('Ad impression.');
        },
      );

      myBanner = BannerAd(
        adUnitId: AppUrls.bannerId,
        size: AdSize.banner,
        request: const AdRequest(),
        listener: listener,
      );

      await myBanner?.load();
    } catch (e) {
      debugPrint(e.toString());
    }
  }

  void _fetchNewJoke() {
    if (jokes.length == 15) {
      return;
    }

    _requestNewJoke();
  }

  Future<void> _requestNewJoke() async {
    final randomId = Random().nextInt(100) + 1;

    final result = await _repository.getRandomJoke(randomId);

    result.fold(
      (l) {
        debugPrint('Error getting the Joke $randomId');
      },
      (r) {
        _addJokeToList(r);
      },
    );

    _fetchNewJoke();
  }

  void _addJokeToList(Joke joke) {
    final jokeIndex = jokes.indexWhere((it) => it.id == joke.id);

    if (jokeIndex != -1) {
      return;
    }

    jokes.add(joke);
    listKey.currentState?.insertItem(jokes.length - 1);

    notifyListeners();
  }

  void onJokeTap(Joke joke, BuildContext context) {
    Navigator.pushNamed(context, RouteNames.singleJoke, arguments: joke);
  }
}
