import 'package:flutter/painting.dart';

class AppLocale {
  AppLocale._();

  static const english = Locale('en');
  static const spanish = Locale('es');

  static List<Locale> get locales => [english, spanish];
}
