import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';

import 'package:chuck_norris_jokes/domain/entities/joke.dart';
import 'package:chuck_norris_jokes/presentation/utils/constants/app_locale.dart';
import 'package:chuck_norris_jokes/presentation/utils/constants/assets_paths.dart';

class ChuckNorrisJokeItem extends StatelessWidget {
  const ChuckNorrisJokeItem({
    super.key,
    required this.joke,
    required this.onTap,
  });

  final Joke joke;
  final Function(Joke) onTap;

  @override
  Widget build(BuildContext context) {
    final shape = RoundedRectangleBorder(
      borderRadius: BorderRadius.circular(15.0),
    );

    return Hero(
      tag: joke.id,
      child: Card(
        shape: shape,
        elevation: 4,
        child: ListTile(
          shape: shape,
          onTap: () => onTap(joke),
          contentPadding: const EdgeInsets.fromLTRB(16, 4, 16, 4),
          leading: const Image(
            image: AssetImage(
              AssetPaths.chuckNorris,
            ),
            width: 50,
            height: 50,
          ),
          title: Text(
            (context.locale == AppLocale.english)
                ? joke.content.en
                : joke.content.es,
          ),
        ),
      ),
    );
  }
}
