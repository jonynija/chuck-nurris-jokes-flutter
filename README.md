# Chuck Norris Jokes

This project is a fun and interactive Chuck Norris jokes app created with Flutter. Initially developed with Flutter 2.5.1, it has been migrated to Flutter 3.0.0, and now runs on Flutter 3.22.2. The app is compatible with Android, iOS, MacOS, and Web platforms.

-   [Android Sample](https://play.google.com/store/apps/details?id=com.jixcayau.chuckNorrisJokes)
-   [Web Sample](https://chuck-norris-jokes-d32a8.web.app)

## Versions to Use

To ensure compatibility and smooth operation, please use the following versions or above of the tools:

-   **Flutter**: 3.22.2
-   **Android Studio**: Android Studio Flamingo
-   **Xcode**: 15

## How to Run

To run the project locally, follow these steps:

1. Clone the repository to your local machine.
2. Open a terminal and navigate to the project directory.
3. Run the following commands:

    ```sh
    flutter pub get
    flutter run
    ```

Alternatively, you can open the project in your favorite editor (such as Android Studio or Visual Studio Code) and run it from there.

## Screenshots

Here are some screenshots of the app in action:

<img src="readme/main.png" width="300" alt="Main screen">
<img src="readme/jokes_list.png" width="300" alt="Jokes List screen">
<img src="readme/joke.png" width="300" alt="Joke screen">

## Developer

For any issues, questions, or further information, feel free to contact me through [LinkedIn](https://www.linkedin.com/in/jonathanixcayau/).
