// ignore_for_file: subtype_of_sealed_class

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import 'package:chuck_norris_jokes/core/type_defs.dart';
import 'package:chuck_norris_jokes/data/datasources/chuck_norris_jokes_data_source.dart';
import 'package:chuck_norris_jokes/data/model/joke_model.dart';

class MockFirebaseFirestore extends Mock implements FirebaseFirestore {}

class MockCollectionReference extends Mock
    implements CollectionReference<Json> {}

class MockDocumentrefence extends Mock implements DocumentReference<Json> {}

class MockDocumentSnapshot extends Mock implements DocumentSnapshot<Json> {}

void main() {
  /*
  * Helper fields
  */

  const jokeContent =
      "u know how there r ufo sightings those arent ufo's those r frisbees Chuck Norris threw";

  /*
  * Vars
  */
  late ChuckNorrisJokesDataSource dataSource;
  late FirebaseFirestore firestore;

  setUpAll(
    () {
      firestore = MockFirebaseFirestore();
      dataSource = ChuckNorrisJokesDataSourceImpl(
        firestore: firestore,
      );
    },
  );

  setUp(() {
    // When
    final collectionReference = MockCollectionReference();
    final documentReference = MockDocumentrefence();
    final documentSnapshot = MockDocumentSnapshot();

    when(
      () => firestore.collection(any()),
    ).thenReturn(
      collectionReference,
    );

    when(
      () => collectionReference.doc(any()),
    ).thenReturn(
      documentReference,
    );

    when(
      () => documentReference.get(),
    ).thenAnswer(
      (_) async => documentSnapshot,
    );

    when(
      () => documentSnapshot.data(),
    ).thenReturn(
      {
        'id': 1,
        'joke': {
          'en': jokeContent,
          'es': jokeContent,
        },
      },
    );

    when(
      () => documentSnapshot.exists,
    ).thenReturn(
      true,
    );
  });

  test(
    'verifySuccessResponse_whenRequestJoke_withValidId',
    () async {
      // Then
      final result = await dataSource.requestJoke(1);

      // Expect
      expect(result, isA<JokeModel?>());
      expect(result, isNotNull);
      expect(result!.content.en, jokeContent);
    },
  );

  test(
    'verifyFailureResponse_whenRequestJoke_withInvalidId',
    () async {
      when(
        () => firestore.collection('jokes').doc('999').get(),
      ).thenThrow(
        Exception('Document not found'),
      );

      expect(dataSource.requestJoke(999), throwsException);
    },
  );

  test(
    'verifyFailureResponse_whenRequestJoke_withException',
    () async {
      when(
        () => firestore.collection('jokes').doc(any()).get(),
      ).thenThrow(
        Exception('Oops, something went wrong'),
      );

      expect(dataSource.requestJoke(1), throwsException);
    },
  );
}
