import 'dart:io';

import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import 'package:chuck_norris_jokes/core/exception.dart';
import 'package:chuck_norris_jokes/core/failure.dart';
import 'package:chuck_norris_jokes/data/datasources/chuck_norris_jokes_data_source.dart';
import 'package:chuck_norris_jokes/data/model/joke_model.dart';
import 'package:chuck_norris_jokes/data/repositories/random_joke_repository.dart';
import 'package:chuck_norris_jokes/domain/entities/joke.dart';
import 'package:chuck_norris_jokes/domain/repositories/random_joke_repository.dart';

class MockChuckNorrisJokesDataSource extends Mock
    implements ChuckNorrisJokesDataSource {}

void main() {
  late MockChuckNorrisJokesDataSource dataSource;
  late RandomJokeRepository repository;

  setUp(() {
    dataSource = MockChuckNorrisJokesDataSource();
    repository = RandomJokeRepositoryImpl(datasource: dataSource);
  });

  group('getRandomJoke', () {
    const validId = 1;
    const invalidId = 999;
    const successJoke = JokeModel(id: 1, content: JokeContent(en: '', es: ''));

    test('should return a joke when the call to data source is successful',
        () async {
      // Arrange
      when(
        () => dataSource.requestJoke(validId),
      ).thenAnswer(
        (_) async => successJoke,
      );

      // Act
      final result = await repository.getRandomJoke(validId);

      // Assert
      expect(result, equals(const Right(successJoke)));
      verify(() => dataSource.requestJoke(validId)).called(1);
    });

    test('should return a ServerFailure when the call to data source fails',
        () async {
      // Arrange
      when(
        () => dataSource.requestJoke(invalidId),
      ).thenThrow(
        ServerException(),
      );

      // Act
      final result = await repository.getRandomJoke(invalidId);

      // Assert
      expect(result, equals(const Left(ServerFailure())));
      verify(() => dataSource.requestJoke(invalidId)).called(1);
    });

    test('should return a ConnectionFailure on socket exception', () async {
      // Arrange
      when(
        () => dataSource.requestJoke(validId),
      ).thenThrow(
        const SocketException('No internet'),
      );

      // Act
      final result = await repository.getRandomJoke(validId);

      // Assert
      expect(result, equals(const Left(ConnectionFailure())));
      verify(() => dataSource.requestJoke(validId)).called(1);
    });

    test('should return an UnknowFailure when an unknown exception occurs',
        () async {
      // Arrange
      when(
        () => dataSource.requestJoke(validId),
      ).thenThrow(
        Exception(),
      );

      // Act
      final result = await repository.getRandomJoke(validId);

      // Assert
      expect(result, equals(const Left(UnknowFailure())));
      verify(() => dataSource.requestJoke(validId)).called(1);
    });
  });
}
