import 'dart:async';
import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import 'package:easy_localization/easy_localization.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:sentry_flutter/sentry_flutter.dart';
import 'package:url_strategy/url_strategy.dart';

import 'package:chuck_norris_jokes/app.dart';
import 'package:chuck_norris_jokes/core/constants.dart';
import 'package:chuck_norris_jokes/firebase_options.dart';
import 'package:chuck_norris_jokes/presentation/utils/constants/app_locale.dart';

void main() async {
  runZonedGuarded(
    () async {
      WidgetsFlutterBinding.ensureInitialized();
      setPathUrlStrategy();

      await SentryFlutter.init(
        (options) {
          options.dsn = AppUrls.sentryUrl;
        },
      );

      await EasyLocalization.ensureInitialized();

      if (!kIsWeb && Platform.isAndroid) {
        MobileAds.instance.initialize();
      }

      await Firebase.initializeApp(
        options: DefaultFirebaseOptions.currentPlatform,
      );

      runApp(
        EasyLocalization(
          path: 'assets/translations',
          fallbackLocale: AppLocale.english,
          supportedLocales: const [
            AppLocale.english,
            AppLocale.spanish,
          ],
          child: const MyApp(),
        ),
      );
    },
    (exception, stack) async {
      await Sentry.captureException(exception, stackTrace: stack);
    },
  );
}
