import 'dart:io';

import 'package:dartz/dartz.dart';

import 'package:chuck_norris_jokes/core/exception.dart';
import 'package:chuck_norris_jokes/core/failure.dart';
import 'package:chuck_norris_jokes/data/datasources/chuck_norris_jokes_data_source.dart';
import 'package:chuck_norris_jokes/domain/entities/joke.dart';
import 'package:chuck_norris_jokes/domain/repositories/random_joke_repository.dart';

class RandomJokeRepositoryImpl extends RandomJokeRepository {
  RandomJokeRepositoryImpl({
    required ChuckNorrisJokesDataSource datasource,
  }) : _datasource = datasource;

  final ChuckNorrisJokesDataSource _datasource;

  @override
  Future<Either<Failure, Joke>> getRandomJoke(int id) async {
    try {
      final result = await _datasource.requestJoke(id);

      if (result != null) {
        return Right(result);
      }
    } on ServerException {
      return const Left(ServerFailure());
    } on SocketException {
      return const Left(ConnectionFailure());
    } catch (e) {
      return const Left(UnknowFailure());
    }

    return const Left(UnknowFailure());
  }
}
