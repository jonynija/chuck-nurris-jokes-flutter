import 'package:chuck_norris_jokes/core/type_defs.dart';
import 'package:chuck_norris_jokes/domain/entities/joke.dart';

class JokeModel extends Joke {
  const JokeModel({
    required super.id,
    required super.content,
  });

  factory JokeModel.fromJson(Json json) {
    return JokeModel(
      id: json['id'],
      content: JokeContentModel.fromJson(json['joke']),
    );
  }
}

class JokeContentModel extends JokeContent {
  const JokeContentModel({
    required super.en,
    required super.es,
  });

  factory JokeContentModel.fromJson(Json json) {
    return JokeContentModel(
      en: json['en'],
      es: json['es'],
    );
  }
}
