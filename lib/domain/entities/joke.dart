class Joke {
  const Joke({
    required this.id,
    required this.content,
  });

  final int id;
  final JokeContent content;
}

class JokeContent {
  const JokeContent({
    required this.en,
    required this.es,
  });

  final String es;
  final String en;
}
