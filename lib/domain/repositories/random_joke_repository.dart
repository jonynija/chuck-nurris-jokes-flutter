import 'package:dartz/dartz.dart';

import 'package:chuck_norris_jokes/core/failure.dart';
import 'package:chuck_norris_jokes/domain/entities/joke.dart';

abstract class RandomJokeRepository {
  Future<Either<Failure, Joke>> getRandomJoke(int id);
}
