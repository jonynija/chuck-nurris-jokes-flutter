abstract class Failure {
  const Failure({
    required this.message,
  });

  final String message;
}

class ServerFailure extends Failure {
  const ServerFailure({
    super.message = 'An error occurred on the server',
  });
}

class ConnectionFailure extends Failure {
  const ConnectionFailure({
    super.message = 'Failed to connect to the network',
  });
}

class UnknowFailure extends Failure {
  const UnknowFailure({
    super.message = 'Unknow Failure',
  });
}
