import 'package:flutter/widgets.dart';

import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:chuck_norris_jokes/core/type_defs.dart';
import 'package:chuck_norris_jokes/data/model/joke_model.dart';

abstract class ChuckNorrisJokesDataSource {
  Future<JokeModel?> requestJoke(int id);
}

class ChuckNorrisJokesDataSourceImpl extends ChuckNorrisJokesDataSource {
  ChuckNorrisJokesDataSourceImpl({
    required FirebaseFirestore firestore,
  }) : _firestore = firestore;

  final FirebaseFirestore _firestore;
  CollectionReference<Json> get _collection => _firestore.collection('jokes');

  @override
  Future<JokeModel?> requestJoke(int id) async {
    try {
      final doc = await _collection.doc('$id').get();

      if (doc.exists && doc.data() != null) {
        return JokeModel.fromJson(doc.data() as Json);
      }
    } catch (e) {
      debugPrint(e.toString());

      rethrow;
    }

    throw Exception("Can't handle result");
  }
}
